# Architecture

The following is a brief description of the API architecture I used:

### Framework

The test API is built using Lumen 8.2.4 and using PHP version 7.3. My approach is to utilise core functionality of the framework without tying the solution into the framework.

### REST API

REST APIs were provided. I adhere to the REST guidelines providing a consistent API with correct response status codes.

### Domain Driven Development

The application code has been developed to try and use a domain driven architecture but not fully utilised because it will be an overkill for the test. Domain concepts are partitioned into their own domain (i.e. Challenge within this test codebase) and within the challenge domain I broke the code into specific layers.

-   Presentation directory for code that sits on the outer most boundary such as HTTP controllers i.e OperationController.

-   Application directory for utility/data handling classes.

-   Domain directory for the test's core business logic.
