## Getting Started

A recent version of Docker and Docker Compose is required for this project to run.

### Services

This application contains two services:

-   Nginx - Nginx application service
-   App - PHP service containing the application code

### Cloning the application

Navigate to your desired directory and run:

```bash
git clone https://gitlab.com/olaleye.osunsanya/league-backend-challenge.git
```

### Running the Services

Navigate to the root of the project and run:

```bash
docker-compose up -d --build
```

If this is the first time this has been run, the project image can take some time to build.

On first run, execute the following command from the root of the project. This will install the required PHP dependencies and execute the required migrations:

```bash
docker-compose exec app composer install
```

### Running the solution

Open Postman or your favourite API application, under form-data, create a key called csv_upload and run the following POST endpoints

To echo

```bash
http://localhost:8080/echo
```

To flatten

```bash
http://localhost:8080/flatten
```

To invert

```bash
http://localhost:8080/invert
```

To sum

```bash
http://localhost:8080/sum
```

To multiply

```bash
http://localhost:8080/multiply
```

### Running all the Tests I include

Test scripts have been included in the application. When the system is running, run the following command from the root of the project:

```bash
docker-compose exec app ./vendor/bin/phpunit
```

To exec into the running PHP application container:

```bash
docker-compose exec app sh
```

To stop running the system, run:

```bash
docker-compose down
```
