<?php

namespace League\Challenge\Application;

use Carbon\Exceptions\InvalidTypeException;
use Illuminate\Http\UploadedFile;

class CSV
{
    /**
     * @var UploadedFile|UploadedFile[]|array|null
     */
    private $uploadedFile;
    /**
     * @var array
     */
    private $data;

    public function __construct(UploadedFile $uploadedFile)
    {
        $this->uploadedFile = $uploadedFile;
    }

    /**
     * @return array
     * @throws InvalidTypeException
     */
    public function getData(): array
    {
        if ($this->uploadedFile->getSize() > 0) {
            $file = fopen($this->uploadedFile, 'r');

            while (($row = fgetcsv($file, 0, ",")) !== FALSE) {
                $this->validate($row);
                $this->data[] = $row;
            }

            fclose($file);
        }

        return is_null($this->data) ? [] : $this->data;
    }

    private function validate(array $row): void
    {
        foreach ($row as $num) {
            if (!is_numeric($num)) {
                throw new InvalidTypeException("{$num} is not a valid integer");
            }
        }
    }
}
