<?php

namespace League\Challenge\Application\Provider;

use League\Challenge\Domain\MatrixOperation;
use League\Challenge\Domain\Operation;
use Illuminate\Support\ServiceProvider;

class ChallengeProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(Operation::class, MatrixOperation::class);
    }
}
