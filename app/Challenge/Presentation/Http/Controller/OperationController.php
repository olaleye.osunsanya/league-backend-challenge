<?php

namespace League\Challenge\Presentation\Http\Controller;

use League\Challenge\Application\CSV;
use League\Challenge\Domain\Operation;
use Carbon\Exceptions\InvalidTypeException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OperationController
{
    /**
     * @var CSV
     */
    private $csv;
    /**
     * @var Operation
     */
    private $matrixOperation;
    /**
     * @var array
     */
    private $data = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Operation $matrixOperation)
    {
        $this->matrixOperation = $matrixOperation;
    }

    /**
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function echo(Request $request): Response
    {
        $response = $this->validateData($request);

        if (!is_null($response)) {
            return $response;
        }

        $body = $this->matrixOperation->echo($this->data);

        return new Response($body, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function invert(Request $request): Response
    {
        $response = $this->validateData($request);

        if (!is_null($response)) {
            return $response;
        }

        $body = $this->matrixOperation->invert($this->data);

        return new Response($body, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function flatten(Request $request): Response
    {
        $response = $this->validateData($request);

        if (!is_null($response)) {
            return $response;
        }

        $body = $this->matrixOperation->flatten($this->data);

        return new Response($body, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function sum(Request $request): Response
    {
        $response = $this->validateData($request);

        if (!is_null($response)) {
            return $response;
        }

        $body = $this->matrixOperation->sum($this->data);

        return new Response($body, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function multiply(Request $request): Response
    {
        $response = $this->validateData($request);

        if (!is_null($response)) {
            return $response;
        }

        $body = $this->matrixOperation->multiply($this->data);

        return new Response($body, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return Response|null
     * @throws Exception
     */
    private function validateData(Request $request): ?Response
    {
        $file = $request->file('csv_upload');

        if (empty($file)) {
            return new Response('Please upload a file', Response::HTTP_UNPROCESSABLE_ENTITY, []);
        }

        if ($file->getClientOriginalExtension() !== 'csv' && $file->getExtension() !== 'txt') {
            return new Response('Please upload a valid CSV file', Response::HTTP_UNPROCESSABLE_ENTITY, []);
        }

        try {
            $this->csv = new CSV($file);
            $this->data = $this->csv->getData();
        } catch (InvalidTypeException $e) {
            return new Response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY, []);
        }

        return null;
    }
}
