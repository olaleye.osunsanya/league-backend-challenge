<?php

namespace League\Challenge\Domain;

use RecursiveArrayIterator;
use RecursiveIteratorIterator;

class MatrixOperation implements Operation
{
    /**
     * @var string
     */
    private $output = '';

    public function echo(array $data): string
    {
        foreach ($data as $row) {
            $this->output .=  sprintf("%s\n", $this->transform($row));
        }

        return $this->output;
    }

    public function invert(array $data): string
    {
        $row = [];

        for ($i = 0; $i < count($data); $i++) {
            for ($j = 0; $j < count($data[$i]); $j++) {
                array_push($row, $data[$j][$i]);
            }
            $this->output .= sprintf("%s\n", $this->transform($row));
            $row = [];
        }

        return $this->output;
    }

    public function flatten(array $data): string
    {
        $singleArray = iterator_to_array(
            new RecursiveIteratorIterator(
                new RecursiveArrayIterator($data)
            ),
            false
        );

        return $this->output .= $this->transform($singleArray);
    }

    public function sum(array $data): int
    {
        $sum = 0;

        for ($i = 0; $i < count($data); $i++) {
            for ($j = 0; $j < count($data[$i]); $j++) {
                $sum += $data[$i][$j];
            }
        }

        return $sum;
    }

    public function multiply(array $data): int
    {
        $product = 1;

        for ($i = 0; $i < count($data); $i++) {
            for ($j = 0; $j < count($data[$i]); $j++) {
                $product *= $data[$i][$j];
            }
        }

        return $data == [] ? 0 : $product;
    }

    private function transform(array $data): string
    {
        return implode(",", $data);
    }
}
