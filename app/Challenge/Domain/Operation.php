<?php

namespace League\Challenge\Domain;

interface Operation
{
    public function echo(array $data): string;
    public function invert(array $data): string;
    public function flatten(array $data): string;
    public function sum(array $data): int;
    public function multiply(array $data): int;
}
