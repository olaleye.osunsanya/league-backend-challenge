<?php

namespace Tests\Challenge\Unit\Application;

use League\Challenge\Application\CSV;
use Carbon\Exceptions\InvalidTypeException;
use Illuminate\Http\UploadedFile;
use PHPUnit\Framework\TestCase;

class CSVTest extends TestCase
{
    /**
     * @var string
     */
    private $filePath;
    /**
     * @var UploadedFile|UploadedFile[]|array|null
     */
    private $uploadedFile;
    /**
     * @var CSV
     */
    private $csv;
    /**
     * @var string
     */
    private $fileName = 'matric.csv';

    public function test_getData_returns_an_array_containing_the_csv_data(): void
    {
        $row1 = '1, 2, 3';
        $row2 = '4, 5, 6';
        $row3 = '7, 8, 9';

        $this->createContent("\n", [$row1, $row2, $row3]);

        $this->uploadedFile = new UploadedFile($this->filePath, $this->fileName, null, null, false);

        $this->csv = new CSV($this->uploadedFile);

        $expected = [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]
        ];

        $actual = $this->csv->getData();

        $this->assertEquals($expected, $actual);
    }

    public function test_getData_throws_an_invalidTypeException_when_a_non_numeric_is_passed(): void
    {
        $row1 = '1, 2, 3';
        $row2 = '4, S, 6';
        $row3 = '7, 8, 9';

        $this->createContent("\n", [$row1, $row2, $row3]);

        $this->uploadedFile = new UploadedFile($this->filePath, $this->fileName, null, null, false);

        $this->csv = new CSV($this->uploadedFile);

        $this->expectException(InvalidTypeException::class);
        $this->expectExceptionMessage('S is not a valid integer');

        $this->csv->getData();
    }

    public function test_getData_returns_an_empty_array_when_an_empty_file_is_uploaded(): void
    {
        $this->createContent("\n", []);

        $this->uploadedFile = new UploadedFile($this->filePath, $this->fileName, null, null, false);

        $this->csv = new CSV($this->uploadedFile);

        $expected = [];

        $actual = $this->csv->getData();

        $this->assertEquals($expected, $actual);
    }

    private function createContent(string $newLine, array $data = []): void
    {
        $this->filePath = '/tmp/matrix.csv';
        $content = implode($newLine, $data);
        file_put_contents($this->filePath, $content);
    }
}
