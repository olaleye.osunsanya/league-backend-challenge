<?php

namespace Tests\Challenge\Unit\Domain;

use League\Challenge\Domain\MatrixOperation;
use PHPUnit\Framework\TestCase;

class MatrixOperationTest extends TestCase
{
    /**
     * @var MatrixOperation
     */
    private $matrixOperation;

    public function setup(): void
    {
        $this->matrixOperation = new MatrixOperation();
    }

    public function test_echo_returns_the_matrix_in_the_string_on_new_lines(): void
    {
        $data = $this->createArray();

        $expected = "1,2,3\n4,5,6\n7,8,9\n";

        $actual = $this->matrixOperation->echo($data);

        $this->assertEquals($expected, $actual);
    }

    public function test_echo_returns_an_empty_string_when_an_empty_array_is_passed(): void
    {
        $this->assertEmpty($this->matrixOperation->echo([]));
    }

    public function test_flatten_returns_the_matrix_on_a_line(): void
    {
        $data = $this->createArray();

        $expected = "1,2,3,4,5,6,7,8,9";

        $actual = $this->matrixOperation->flatten($data);

        $this->assertEquals($expected, $actual);
    }

    public function test_flatten_returns_an_empty_string_when_an_empty_array_is_passed(): void
    {
        $data = [];

        $expected = '';

        $this->assertEquals($expected, $this->matrixOperation->flatten($data));
    }

    public function test_invert_returns_an_inverted_array_in_string_format(): void
    {
        $data = $this->createArray();

        $expected = "1,4,7\n2,5,8\n3,6,9\n";

        $actual = $this->matrixOperation->invert($data);

        $this->assertEquals($expected, $actual);
    }

    public function test_invert_returns_an_empty_string_when_an_empty_array_is_passed(): void
    {
        $data = [];

        $expected = '';

        $this->assertEquals($expected, $this->matrixOperation->invert($data));
    }

    public function test_sum_returns_the_summation_of_all_elements_in_array(): void
    {
        $data = $this->createArray();

        $expected = 45;

        $actual = $this->matrixOperation->sum($data);

        $this->assertEquals($expected, $actual);
    }

    public function test_sum_returns_zero_when_an_empty_array_is_passed(): void
    {
        $data = [];

        $expected = 0;

        $this->assertEquals($expected, $this->matrixOperation->sum($data));
    }

    public function test_multiply_returns_the_product_of_all_elements_in_array(): void
    {
        $data = $this->createArray();

        $expected = 362880;

        $actual = $this->matrixOperation->multiply($data);

        $this->assertEquals($expected, $actual);
    }

    public function test_multiply_returns_zero_when_an_empty_array_is_passed(): void
    {
        $data = [];

        $expected = 0;

        $this->assertEquals($expected, $this->matrixOperation->multiply($data));
    }

    private function createArray(): array
    {
        return [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]
        ];
    }
}
