<?php

namespace Tests\Challenge\Unit\Presentation\Http\Controller;

use League\Challenge\Domain\MatrixOperation;
use League\Challenge\Domain\Operation;
use League\Challenge\Presentation\Http\Controller\OperationController;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use PHPUnit\Framework\TestCase;

class OperationControllerTest extends TestCase
{
    /**
     * @var Operation
     */
    private $matrixOperation;
    /**
     * @var OperationController
     */
    private $controller;
    /**
     * @var string
     */
    private $filePath;
    /**
     * @var string
     */
    //private $fileName = 'matric.csv';
    /**
     * @var array
     */
    private $body = [];

    public function setUp(): void
    {
        $this->matrixOperation = new MatrixOperation();
        $this->controller = new OperationController($this->matrixOperation);
    }

    public function test_echo_returns_200_response_containing_the_string_data(): void
    {
        $row1 = '1,2,3';
        $row2 = '4,5,6';
        $row3 = '7,8,9';

        $this->createContent("\n", [$row1, $row2, $row3]);

        $request = Request::create('/echo', 'POST', [], [], $this->body, [], null);

        $response = $this->controller->echo($request);

        $expectedBody = "1,2,3\n4,5,6\n7,8,9\n";

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($expectedBody, $response->getContent());
    }

    public function test_echo_returns_422_response_when_an_invalid_element_is_in_array(): void
    {
        $row1 = '1,2,3';
        $row2 = '4,s,6';
        $row3 = '7,8,9';

        $this->createContent("\n", [$row1, $row2, $row3]);

        $request = Request::create('/echo', 'POST', [], [], $this->body, [], null);

        $response = $this->controller->echo($request);

        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals('s is not a valid integer', $response->getContent());
    }

    public function test_echo_returns_422_and_a_response_when_file_is_empty(): void
    {
        $request = Request::create('/echo', 'POST', [], [], [], [], null);

        $response = $this->controller->echo($request);

        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals('Please upload a file', $response->getContent());
    }

    public function test_invert_returns_200_response_containing_the_string_data(): void
    {
        $row1 = '1,2,3';
        $row2 = '4,5,6';
        $row3 = '7,8,9';

        $this->createContent("\n", [$row1, $row2, $row3]);

        $request = Request::create('/invert', 'POST', [], [], $this->body, [], null);

        $response = $this->controller->invert($request);

        $expectedBody = "1,4,7\n2,5,8\n3,6,9\n";

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($expectedBody, $response->getContent());
    }

    public function test_invert_returns_422_response_when_an_invalid_element_is_in_array(): void
    {
        $row1 = '1,2,3';
        $row2 = '4,s,6';
        $row3 = '7,8,9';

        $this->createContent("\n", [$row1, $row2, $row3]);

        $request = Request::create('/invert', 'POST', [], [], $this->body, [], null);

        $response = $this->controller->invert($request);

        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals('s is not a valid integer', $response->getContent());
    }

    public function test_invert_returns_422_and_a_response_when_file_is_empty(): void
    {
        $request = Request::create('/invert', 'POST', [], [], [], [], null);

        $response = $this->controller->invert($request);

        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals('Please upload a file', $response->getContent());
    }

    public function test_flatten_returns_200_response_containing_the_string_data(): void
    {
        $row1 = '1,2,3';
        $row2 = '4,5,6';
        $row3 = '7,8,9';

        $this->createContent("\n", [$row1, $row2, $row3]);

        $request = Request::create('/flatten', 'POST', [], [], $this->body, [], null);

        $response = $this->controller->flatten($request);

        $expectedBody = "1,2,3,4,5,6,7,8,9";

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($expectedBody, $response->getContent());
    }

    public function test_flatten_returns_422_response_when_an_invalid_element_is_in_array(): void
    {
        $row1 = '1,2,3';
        $row2 = '4,s,6';
        $row3 = '7,8,9';

        $this->createContent("\n", [$row1, $row2, $row3]);

        $request = Request::create('/flatten', 'POST', [], [], $this->body, [], null);

        $response = $this->controller->flatten($request);

        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals('s is not a valid integer', $response->getContent());
    }

    public function test_flatten_returns_422_and_a_response_when_file_is_empty(): void
    {
        $request = Request::create('/flatten', 'POST', [], [], [], [], null);

        $response = $this->controller->flatten($request);

        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals('Please upload a file', $response->getContent());
    }

    public function test_sum_returns_200_response_containing_the_string_data(): void
    {
        $row1 = '1,2,3';
        $row2 = '4,5,6';
        $row3 = '7,8,9';

        $this->createContent("\n", [$row1, $row2, $row3]);

        $request = Request::create('/sum', 'POST', [], [], $this->body, [], null);

        $response = $this->controller->sum($request);

        $expectedBody = 45;

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($expectedBody, $response->getContent());
    }

    public function test_sum_returns_422_response_when_an_invalid_element_is_in_array(): void
    {
        $row1 = '1,2,3';
        $row2 = '4,s,6';
        $row3 = '7,8,9';

        $this->createContent("\n", [$row1, $row2, $row3]);

        $request = Request::create('/sum', 'POST', [], [], $this->body, [], null);

        $response = $this->controller->sum($request);

        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals('s is not a valid integer', $response->getContent());
    }

    public function test_sum_returns_422_and_a_response_when_file_is_empty(): void
    {
        $request = Request::create('/sum', 'POST', [], [], [], [], null);

        $response = $this->controller->sum($request);

        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals('Please upload a file', $response->getContent());
    }

    public function test_multiply_returns_200_response_containing_the_string_data(): void
    {
        $row1 = '1,2,3';
        $row2 = '4,5,6';
        $row3 = '7,8,9';

        $this->createContent("\n", [$row1, $row2, $row3]);

        $request = Request::create('/multiply', 'POST', [], [], $this->body, [], null);

        $response = $this->controller->multiply($request);

        $expectedBody = 362880;

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($expectedBody, $response->getContent());
    }

    public function test_multiply_returns_422_response_when_an_invalid_element_is_in_array(): void
    {
        $row1 = '1,2,3';
        $row2 = '4,s,6';
        $row3 = '7,8,9';

        $this->createContent("\n", [$row1, $row2, $row3]);

        $request = Request::create('/multiply', 'POST', [], [], $this->body, [], null);

        $response = $this->controller->multiply($request);

        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals('s is not a valid integer', $response->getContent());
    }

    public function test_multiply_returns_422_and_a_response_when_file_is_empty(): void
    {
        $request = Request::create('/multiply', 'POST', [], [], [], [], null);

        $response = $this->controller->multiply($request);

        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals('Please upload a file', $response->getContent());
    }

    public function test_a_422_is_returned_and_a_response_when_file_is_not_a_csv(): void
    {
        $row1 = '1,2,3';
        $row2 = '4,s,6';
        $row3 = '7,8,9';

        $this->createContent("\n", [$row1, $row2, $row3], 'test.docx');

        $request = Request::create('/multiply', 'POST', [], [], $this->body, [], null);

        $response = $this->controller->multiply($request);

        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals('Please upload a valid CSV file', $response->getContent());
    }

    private function createContent(string $newLine, array $data = [], $fileName = 'matric.csv'): void
    {
        $this->filePath = '/tmp/matrix.csv';

        $content = implode($newLine, $data);

        file_put_contents($this->filePath, $content);

        $this->uploadedFile = new UploadedFile($this->filePath, $fileName, null, null, false);

        $this->body = [
            'csv_upload' => $this->uploadedFile
        ];
    }
}
