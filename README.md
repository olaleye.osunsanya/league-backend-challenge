# League Backend Developer Challenge API Test

## Assignment Requirements

Please clone this repository to begin.

### Cloning the application

Navigate to your desired directory and run:

```bash
git clone https://gitlab.com/olaleye.osunsanya/league-backend-challenge.git
```

Read [Getting Started](docs/getting_started.md) to create a working local environment then read the following documents to gain an understanding of the architecture and development approach:

-   [Architecture](docs/architecture.md)
