<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use App\Challenge\Presentation\Http\Controller\OperationController;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group([], function () use ($router) {
    $router->post('/echo', 'OperationController@echo');
    $router->post('/invert', 'OperationController@invert');
    $router->post('/flatten', 'OperationController@flatten');
    $router->post('/sum', 'OperationController@sum');
    $router->post('/multiply', 'OperationController@multiply');
});
