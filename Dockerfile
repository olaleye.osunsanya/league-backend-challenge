FROM php:7.3.5-fpm-alpine

LABEL maintainer="Olaleye Osunsanya"
LABEL description="League Backend Challenge"

ARG uid=1000
ARG user=olaleye

# Install system dependencies
RUN apk add --update && apk add --no-cache \
    git \
    curl \
    libpng-dev \
    libxml2-dev \
    zip \
    unzip

# Install PHP extensions
RUN docker-php-ext-install mbstring exif pcntl bcmath gd

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

RUN adduser -u $uid -G www-data -h /home/$user -D $user

RUN mkdir -p /home/$user/.composer && \
    chown -R $user /home/$user

# Set working directory
WORKDIR /var/www

USER $user